# Projeto para Reservar Hosteis

### Descrição

    O site realiza reservas simplificadas de hosteis.


### Estrutura dos dados

    #### Quartos
        -description
        -name
        -reservations
        -status

    #### Reservas
        -name
        -email
        -data_nascimento
        -quarto_id