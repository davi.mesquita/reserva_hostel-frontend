import http from '../config/http';

const getServiceAllQuartos = () => http.get('/quarto');
const getServiceReserva = (quartoId) => http.get(`/quarto/${quartoId}`);
const createServiceQuarto = (quartoId, data) => http.post(`quarto/${quartoId}/reserva`, data)
const deleteServiceReserva = (idReserva) => http.delete(`/reserva/${idReserva}`)


export {
    getServiceAllQuartos,
    getServiceReserva,
    createServiceQuarto,
    deleteServiceReserva
}

