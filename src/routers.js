import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";

// layout
import Layout from './components/layout'

// views
import Quartos from './views/quartos';
import Reservas from './views/reservas';
import Sobre from './views/sobre'
import Error404 from './views/errors/404';


const Routers = () => {

    return (
        <Router>
            <Layout nomeDaPagina="Hostel Primariqui">
                <Switch>
                    <Route exact path='/' component={Quartos} />
                    <Route exact path='/quartos/:id' component={Reservas} />
                    <Route exact path='/sobre' component={Sobre} />
                    <Route exact to="/error/404" component={Error404} />
                    <Redirect from="*" to="/error/404" />

                </Switch>
            </Layout>
        </Router >
    )

}


export default Routers
