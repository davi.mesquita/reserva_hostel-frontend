import React from 'react';
import ReactDOM from 'react-dom';
import GlobalStyled from './assets/globalStyled';
import 'bootstrap/dist/css/bootstrap.min.css';

import Routers from './routers'

ReactDOM.render(
  <React.Fragment>
    <GlobalStyled />
    <Routers />
  </React.Fragment>,
  document.getElementById('root')
);