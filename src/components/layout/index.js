import Header from './header'
import Footer from './footer'
import styled from 'styled-components';

const Layout = (props) => {
    document.title = props.nomeDaPagina;
    return (
        <>
            <Header titulo={props.nomeDaPagina} />
            <Main classname="container">
                {props.children}
            </Main>
            <Footer />
        </>
    )
}

export default Layout;

const Main = styled.main`
    flex:1;
    background-color: #CCC9A1;
    padding: 20px 0;
`