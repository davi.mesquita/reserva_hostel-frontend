import styled from "styled-components";
import { BiHappyAlt } from 'react-icons/bi';

const Footer = () => (
    <SFooter>
        <FooterP>Feito por Davi e Kadu <HappyIcon/></FooterP>
    </SFooter>
);

export default Footer;


const SFooter = styled.footer`
    border-top: 2px solid #E9935D;
    text-align:center;
    padding: 10px;
    background-color: #A53F2B;
`

const HappyIcon = styled(BiHappyAlt)`
    font-size: 20px;
    margin-top: -4px`

const FooterP = styled.p`
    color: #F3C3A5;
`