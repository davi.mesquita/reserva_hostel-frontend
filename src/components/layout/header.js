import React, { useState } from 'react';
import { NavLink as RRDNavLink } from 'react-router-dom'; // funcionalidade
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink, 
    Container,
    Tooltip
} from 'reactstrap';
import styled from 'styled-components';
import { FiKey } from 'react-icons/fi'

const Header = (props) => {
    const [isOpen, setIsOpen] = useState(false);
    const [tooltipOpen, setTooltipOpen] = useState(false);

    const toggleTooltip = () => setTooltipOpen(!tooltipOpen);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <header>
            <SNavbar color="dark" dark expand="md">
                <Container>
                    <NavbarBrand tag={RRDNavLink} to="/" id="logoMain"> <Sp><IconLogo /> Hostel Primariqui</Sp></NavbarBrand>
                    <Tooltip placement="top" isOpen={tooltipOpen} autohide={false} target="logoMain" toggle={toggleTooltip}>
                        Voltar ao Menu Principal
                    </Tooltip>
                    <NavbarToggler onClick={toggle} />
                    <SCollapse isOpen={isOpen} navbar>
                        <Nav className="mr-auto" navbar>
                            <NavItem>
                                <SNavLink exact tag={RRDNavLink} activeClassName="active" to="/" >Quartos</SNavLink>
                            </NavItem>
                            <NavItem >
                                <SNavLink exact tag={RRDNavLink} activeClassName="active" to="/sobre" >Sobre</SNavLink>
                            </NavItem>
                        </Nav>
                    </SCollapse>
                </Container>
            </SNavbar>
        </header>
    )
}

export default Header


const SNavbar = styled(Navbar)`
    background-color: #520008 !important;
    border-bottom: 5px solid #A53F2B;

    a {
        color: #F0B793 !important;
    }

`

const SNavLink = styled(NavLink)`
    margin: auto 5px;
    border-radius: 5px;
    color: #F3C3A5;

    &.active {
        color: #fff !important;
        background-color: #A53F2B !important;
    }

    @media (max-width: 767.98px) {
        margin:6px 0;
        
    }

`
const SCollapse = styled(Collapse)`
    flex-grow: 0;
`

const Sp = styled.s`
    color: #F3C3A5;
    text-decoration: none;
`

const IconLogo = styled(FiKey)`
    font-size: 26px;
    margin-top: -4px;
    color: #F1B416;
`

