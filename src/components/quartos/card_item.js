import { Link } from 'react-router-dom';
import {
    Badge,
    Button,
    Card, CardBody,
    CardImg,
    CardTitle
} from 'reactstrap';
import styled from 'styled-components';
import images from '../../assets/img/quartos.json';

const CardItem = (props) => {
    const { name, id, description } = props.item;
    
    let descSplit = description.split(", ");
    const Descricao = (descSplit) => descSplit.map((item, i) => (
        <SBadge color="danger" key={i}>{item}</SBadge> 
    ))
    return (
        <SCard>
            <CardImg top width="100%" src={images[id-1][0].src} alt={name} />
            <CardBody>
                <CardTitle tag="h5">{name} </CardTitle> 
                {Descricao(descSplit)} <br></br>
                <Button size="sm"  tag={Link} to={`/quartos/${id}`} color="link">Faça sua reserva</Button>
            </CardBody>
        </SCard>
    )
}

export default CardItem;


const SCard =  styled(Card)`

    background-color: #fafafa;

    :hover {
        background-color: #ddd;
        transition:1s;
        opacity: 0.9;
    }

`
const SBadge = styled(Badge)`
    margin-right: 4px;
`
