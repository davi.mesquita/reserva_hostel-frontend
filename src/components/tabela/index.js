import { deleteServiceReserva } from '../../services/quarto.service'
import ReactSwal from '../../plugins/swal'
import { useState } from 'react';
import styled from 'styled-components';
import { BiTrash } from 'react-icons/bi'
import { Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';


const Tabela = ({ reservados, update }) => {
    const [modal, setModal] = useState({
        isOpen: false,
        data: null
    })

    const apagarReserva = () => {
        if(modal.data.id) {
        console.log(modal.data.id);
         deleteServiceReserva(modal.data.id)
         .then(() => {
             ReactSwal.fire({
                 icon: 'success',
                 title: `O Aluno ${modal?.data?.name?.split(" ")[0]} deletado com sucesso!`,
                 showConfirmButton: false,
                 showCloseButton: true
             })
             update(true)
         })   
         .catch(erro => console.log("Falhou no delete"))
        }
    }

    const toggleModal = (data = null) => {
        setModal({
            isOpen: !modal.isOpen,
            data
        })
    }

    return (
        <div>
            {reservados && reservados.length ? (
                <div>
                    <STable responsive striped size="sm">
                        <thead>
                            <TableTr>
                                <th>Nome</th>
                                <th>E-mail</th>
                                <th>Check-in</th>
                                <th>Check-out</th>
                                <th>Ações</th>
                            </TableTr>
                        </thead>
                        <tbody>
                            {reservados && reservados.map((v, i) => (
                                <TableTr key={i}>
                                    <td>{v.name}</td>
                                    <td>{v.email}</td>
                                    <td>{new Date(v.checkin).toLocaleDateString()}</td>
                                    <td>{new Date(v.checkout).toLocaleDateString()}</td>
                                    <td>
                                        <Button alt='Excluir reserva' size="sm" className="text-danger" color="link"
                                            onClick={() => toggleModal(v)} ><BiTrash size="20" /></Button>
                                    </td>
                                </TableTr>
                            ))}
                        </tbody>
                    </STable>

                    <Modal isOpen={modal.isOpen} toggle={toggleModal}>
                        <ModalHeader toggle={toggleModal}>Excluir Reserva</ModalHeader>
                        <ModalBody>
                            Deseja Excluir a Reserva do(a)<strong>{modal?.data?.name?.split(" ")[0]}</strong> ?
                        </ModalBody>
                        <ModalFooter>

                            <Button color="primary" onClick={apagarReserva}>SIM</Button>
                            <Button color="secondary" onClick={toggleModal}>NÃO</Button>
                        </ModalFooter>
                    </Modal>
                </div>
            ) : (
                <div>Não existem alunos cadastrados</div>
            )}
        </div>
    )
}

export default Tabela;


const STable = styled(Table)`
    overflow:hidden;
    border-radius: 4px;
    font-size:14px;
    font-family: 'Roboto', sans-serif;
`
const TableTr = styled.tr`

    th{
        background-color:#666;
        color: #fff;
        :nth-child(n){ min-width: 200px;}
        :nth-child(1){ min-width: 400px;}
        :nth-child(4){ min-width: 100px;text-align: center;}
    }
    
    
    

    td{
        :nth-child(1){ text-transform: uppercase;}
        :nth-child(3){ text-transform: lowercase;}
        :nth-child(4){ text-align: center;  }
    }

`