import { useState } from "react";
import { createServiceQuarto } from "../../services/quarto.service"
import { Button, Row, Col, FormGroup, Label, Input } from 'reactstrap';
import ReactSwal from "../../plugins/swal";

const Reserva = ({ id, update, isForm }) => {
    const [form, setForm] = useState({})

    console.log(id);

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }

    const submitForm = () => {
        const nform = {
            ...form,
            name: form.name.toUpperCase(),
            email: form.email.toLowerCase()
        }

        createServiceQuarto(id, nform)
            .then(() => {
                ReactSwal.fire({
                    icon: 'success',
                    title: `A reserva do(a) ${form.name} foi feita com sucesso.`,
                    showConfirmButton: false,
                    showCloseButton: true,
                })
                setForm({});
                update(true)
                isForm(false)
            })
            .catch(erro => console.log("Houve erro no cadastro de reserva"));
    }

    return (
        <Row>
            <Col xs="12" sm="12" md="8" lg="8">
                <FormGroup>
                    <Label for="name">Nome</Label>
                    <Input type="text" id="name" value={form.name || ""} onChange={handleChange}
                        name="name" placeholder="Insira seu nome" className="text-uppercase" />
                </FormGroup>
                <FormGroup>
                    <Label for="email">Email</Label>
                    <Input type="email" id="email" value={form.email || ""} onChange={handleChange}
                        name="email" placeholder="Insira seu email" className="text-lowercase" />
                </FormGroup>
                <FormGroup>
                    <Label for="nascimento">Check-in</Label>
                    <Input type="date" id="checkin" value={form.checkin || ""} onChange={handleChange}
                        name="checkin" placeholder="Insira a data de check-in" />
                </FormGroup>
                <FormGroup>
                    <Label for="nascimento">Check-out</Label>
                    <Input type="date" id="checkout" value={form.checkout || ""} onChange={handleChange}
                        name="checkout" placeholder="Insira a data de checkout" />
                </FormGroup>
                <FormGroup>
                    <Button color="primary" onClick={submitForm}>Reservar</Button>
                </FormGroup>
            </Col>
        </Row>
    )
}

export default Reserva;