import styled from "styled-components"

const InputField = styled.input`
    width: 100%;
    max-width: 500px;
    border:none;
    background-color: #fafafa;
    padding:10px 5px;
    box-shadow: 1px 1px 1px  #666;
`

export {
    InputField
}