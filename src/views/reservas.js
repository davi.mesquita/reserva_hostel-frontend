import React, { useState, useEffect, useCallback } from 'react';
import { useParams } from "react-router";
import { getServiceReserva } from '../services/quarto.service';
import Loading from '../components/loading';
import Reserva from '../components/reserva';
import TabelaReserva from '../components/tabela';
import { Jumbotron, Button, Col } from 'reactstrap';
import { BsListCheck, BsPlus } from 'react-icons/bs'
import styled from 'styled-components';


const Reservas = (props) => {
    const { id } = useParams();
    const { history } = props;
    const [loading, setLoading] = useState(false);
    const [reserva, setReserva] = useState({});
    const [update, setUpdate] = useState(false);
    const [isSub, setSub] = useState(false);

    const getReservas = useCallback(async () => {
        try {
            setLoading(true)
            const res = await getServiceReserva(id);
            setReserva(res.data)
            setLoading(false)

        } catch (error) {
            console.log('###', error)
            history.push('/?error=404')
        }
    }, [id, history])

    useEffect(() => {
        getReservas();
        setUpdate(false)
    }, [getReservas, update])

    const DetalhaQuarto = ({ name, status, id }) => (
        <SJumbotron>
            <Col>
            <div className="display-4">{name}</div>
            <p className="lead">
                {status ? <strong>Temos quartos disponíveis.</strong> : <strong>Estamos lotados.</strong>}
            </p>
            </Col>
            
            <Col>
               
            </Col>
        </SJumbotron>
    )

    const Menu = () => (
        <Navbar expand="md mb-4">
            <strong className="info d-none d-md-block d-lg-block">
                {isSub ? "Faça aqui a sua reserva" : "Lista de reservas"}
            </strong>
            <Button onClick={() => setSub(!isSub)} color={!isSub ? "danger" : "warning"} size="sm">
                {!isSub ? (<><BsPlus /> Reservas</>) : (<><BsListCheck /> Lista de Reservas</>)}
            </Button>
        </Navbar>
    )

    const montarTela = (detalhe) => (
        <div>
            {DetalhaQuarto(detalhe)}
            {Menu()}
            {
                isSub
                    ? (<Reserva id={id} update={setUpdate} isForm={setSub} />)
                    : (<TabelaReserva reservados={detalhe.reservations} update={setUpdate} />)
            }
        </div>
    )



    return (
        loading
            ? <Loading />
            : montarTela(reserva)
    )

}

export default Reservas;

const Navbar = styled.div`
    background-color:none !important;
    margin: 10px 0 20px;
    padding: 10px 0;
    border-bottom: thin dotted #4446;
    display:flex;
    
    .info {
        flex:1;
    }
`

const SJumbotron = styled(Jumbotron)`
    background-color:#D5715D;
    color: #1A1A0E;
`