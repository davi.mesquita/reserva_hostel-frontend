import React, { useState, useEffect, useCallback } from 'react';
import { getServiceAllQuartos } from '../services/quarto.service.js';
import CardItem from "../components/quartos/card_item";
import Loading from '../components/loading';
import { Col, Container, Row } from 'reactstrap';

const Quartos = () => {

    const [quartos, setQuartos] = useState([]);
    const [loading, setLoading] = useState(false);

    const getQuartos = useCallback(() => {
        setLoading(true);
        getServiceAllQuartos()
            .then(res => {
                setQuartos(res.data);
                setLoading(false);
            })
            .catch(err => {
                console.log("Errou no loading ao puxar os cursos", err);
                setLoading(false);
            })
    }, [])

    useEffect(() => {
        getQuartos();
    }, [getQuartos])


    const MapearQuartos = (quartos) => quartos.map((item, i) => (
        <Col  key={i} className="mb-4">
            <CardItem item={{ ...item, status: true }} />
        </Col>
    ))

    const LoadingNoMeio = () => ( //Gambiarra pro loading ficar no meio mesmo com o row limitado a 3
        <Col sm="12" md={{ size: 6, offset: 3 }}><Loading /></Col>
        
    )

    return (
        <Container>
            <Row xs="3">
                {loading ? LoadingNoMeio() : MapearQuartos(quartos)}
            </Row>
        </Container>
    )
}

export default Quartos;
